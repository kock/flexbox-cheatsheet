import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Divider, Typography } from '@material-ui/core/';
import * as prop from '../../../utils/definitions';
import { inlineCSSStyling, displayCodeBlock } from '../../../utils/functions';
import {
  flexDirection,
  flexWrap,
  justifyContent,
  alignItems,
  alignContent,
} from '../../../utils/AsideData';
import styles from './styles';
import FieldSelect from '../../Fields/FieldSelect';
import InfoButton from '../../Buttons/InfoButton';

const ContainerFields = ({ classes, ...props }) => {
  const { container } = props;
  return (
    <React.Fragment>
      <section>
        <article>
          <div className="style-box-area">
            <style
              className={classes.code}
              contentEditable
              suppressContentEditableWarning
            >
              {displayCodeBlock(inlineCSSStyling(container))}
            </style>
          </div>
        </article>
      </section>

      <Divider />

      <Typography variant="body1" className={classes.subtitles}>
        <InfoButton link="#article-header-id-3" />
        flex-direction: row | row-reverse | column | column-reverse;
      </Typography>

      <form className={classes.col1} noValidate autoComplete="off">
        <FieldSelect
          label="flex-direction"
          type={prop.FLEX_DIRECTION}
          selectItems={flexDirection}
          classes={classes}
          {...props}
        />
      </form>

      <Divider />

      <Typography variant="body1" className={classes.subtitles}>
        <InfoButton link="#article-header-id-4" />
        flex-wrap: nowrap | wrap | wrap-reverse;
      </Typography>
      <form className={classes.col1} noValidate autoComplete="off">
        <FieldSelect
          label="flex-wrap"
          type={prop.FLEX_WRAP}
          selectItems={flexWrap}
          classes={classes}
          {...props}
        />
      </form>

      <Divider />

      <Typography variant="body1" className={classes.subtitles}>
        <InfoButton link="#article-header-id-6" />
        justify-content: flex-start | flex-end | center | space-between |
        space-around | space-evenly;
      </Typography>
      <form className={classes.col1} noValidate autoComplete="off">
        <FieldSelect
          label="justify-content"
          type={prop.JUSTIFY_CONTENT}
          selectItems={justifyContent}
          classes={classes}
          {...props}
        />
      </form>

      <Divider />

      <Typography variant="body1" className={classes.subtitles}>
        <InfoButton link="#article-header-id-7" />
        align-items: flex-start | flex-end | center | baseline | stretch;
      </Typography>
      <form className={classes.col1} noValidate autoComplete="off">
        <FieldSelect
          label="align-items"
          type={prop.ALIGN_ITEMS}
          selectItems={alignItems}
          classes={classes}
          {...props}
        />
      </form>

      <Divider />

      <Typography variant="body1" className={classes.subtitles}>
        <InfoButton link="#article-header-id-8" />
        align-content: flex-start | flex-end | center | space-between |
        space-around | stretch;
      </Typography>
      <form className={classes.col1} noValidate autoComplete="off">
        <FieldSelect
          label="align-content"
          type={prop.ALIGN_CONTENT}
          selectItems={alignContent}
          classes={classes}
          {...props}
        />
      </form>
    </React.Fragment>
  );
};

ContainerFields.propTypes = {
  classes: PropTypes.shape({}).isRequired,
};

export default withStyles(styles)(ContainerFields);
