const styles = theme => ({
  main: {
    gridArea: 'container',
  },
  asideBarContainer: {
    position: 'relative',
    gridArea: 'sidebar',
  },
  drawerPaper: {
    position: 'absolute',
    right: 0,
    top: 0,
    overflowY: 'auto',
    width: '100%',
  },
  toolbar: {
    ...theme.mixins.toolbar,
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  subtitles: {
    margin: '.5em 0',
    whiteSpace: 'pre-line',
  },
  wrapper: {
    padding: '1rem',
    boxSizing: 'border-box',
  },
  expansionPanel: {
    background: '#efefef',
  },
  col1: {
    display: 'flex',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr',
  },
  col2: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gridTemplateRows: '1fr',
  },
  col3: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr 1fr',
    gridTemplateRows: '1fr',
  },
  col4: {
    display: 'flex',
  },
  col5: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr',
    gridTemplateRows: '1fr',
  },
  textField: {
    marginRight: theme.spacing.unit,
    flex: '1',
  },
  textFieldSelect: {
    marginRight: theme.spacing.unit,
    flex: '1',
  },
  button: {
    width: 'auto',
    height: 'auto',
    alignSelf: 'center',
    justifyContent: 'center',
    justifySelf: 'center',
    minHeight: 0,
  },
  infoButton: {
    padding: '0',
    top: '-15px',
  },
  code: {
    display: 'block',
    whiteSpace: 'pre-wrap',
    background: '#3c3b3b',
    color: '#fff',
    padding: '1rem',
    boxSizing: 'border-box',
    fontSize: '0.8rem',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  flex: {
    display: 'flex',
  },
});

export default styles;
