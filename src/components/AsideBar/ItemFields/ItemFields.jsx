import React from 'react';
import { Divider, Typography } from '@material-ui/core/';
import * as prop from '../../../utils/definitions';
import {
  displayCodeBlockItem,
  showNumberInput,
} from '../../../utils/functions';
import { units, alignSelf } from '../../../utils/AsideData';
import FieldNumberItem from '../../Fields/FieldNumberItem';
import FieldSelectItem from '../../Fields/FieldSelectItem';
import InfoButton from '../../Buttons/InfoButton';

class ItemFields extends React.Component {
  updateItemStyled = e => {
    const style = document.createElement('style');
    document.head.appendChild(style);
    style.innerHTML = e.target.innerHTML;
  };

  render() {
    const { classes, items, activeItem, handleChangeItem } = this.props;

    return (
      <React.Fragment>
        <section>
          <article>
            <Typography variant="title" className={classes.subtitles}>
              Selected item = {activeItem + 1}
            </Typography>
            <div className="style-box-area">
              <style
                type="text/css"
                className={classes.code}
                // contentEditable
                // suppressContentEditableWarning={true}
                // onBlur={this.updateItemStyled}
              >
                {displayCodeBlockItem(items[activeItem], activeItem)}
              </style>
            </div>
          </article>
        </section>

        <Divider />

        <Typography variant="body1" className={classes.subtitles}>
          {`height/width:`}
        </Typography>
        <form className={classes.flex} noValidate autoComplete="off">
          {showNumberInput(items[activeItem][prop.FLEX_ITEM_HEIGHT]) && (
            <FieldNumberItem
              label={prop.FLEX_ITEM_HEIGHT_LENGTH}
              value={items[activeItem][prop.FLEX_ITEM_HEIGHT_LENGTH]}
              onChange={handleChangeItem(
                activeItem,
                prop.FLEX_ITEM_HEIGHT_LENGTH,
              )}
              {...this.props}
            />
          )}
          <FieldSelectItem
            className={classes.stretchFlexItem}
            label={prop.FLEX_ITEM_HEIGHT}
            selectItems={units}
            value={items[activeItem][prop.FLEX_ITEM_HEIGHT]}
            onChange={handleChangeItem(activeItem, prop.FLEX_ITEM_HEIGHT)}
            {...this.props}
          />
          {showNumberInput(items[activeItem][prop.FLEX_ITEM_WIDTH]) && (
            <FieldNumberItem
              label={prop.FLEX_ITEM_WIDTH_LENGTH}
              value={items[activeItem][prop.FLEX_ITEM_WIDTH_LENGTH]}
              onChange={handleChangeItem(
                activeItem,
                prop.FLEX_ITEM_WIDTH_LENGTH,
              )}
              {...this.props}
            />
          )}
          <FieldSelectItem
            className={classes.stretchFlexItem}
            label={prop.FLEX_ITEM_WIDTH}
            selectItems={units}
            value={items[activeItem][prop.FLEX_ITEM_WIDTH]}
            onChange={handleChangeItem(activeItem, prop.FLEX_ITEM_WIDTH)}
            {...this.props}
          />
        </form>

        <Typography variant="body1" className={classes.subtitles}>
          <InfoButton link="#article-header-id-9" />
          {`order: <integer />; /* default 0 */`}
        </Typography>
        <form className={classes.flex} noValidate autoComplete="off">
          <FieldNumberItem
            label={prop.ORDER}
            value={items[activeItem][prop.ORDER]}
            onChange={handleChangeItem(activeItem, prop.ORDER)}
            {...this.props}
          />
        </form>

        <Typography variant="body1" className={classes.subtitles}>
          <InfoButton link="#article-header-id-10" />
          {`flex-grow: <number />; /* default 0 */`}
          <InfoButton link="#article-header-id-11" />
          {`flex-shrink: <number />; /* default 1 */`}
        </Typography>
        <form className={classes.col2} noValidate autoComplete="off">
          <FieldNumberItem
            label={prop.FLEX_GROW}
            value={items[activeItem][prop.FLEX_GROW]}
            onChange={handleChangeItem(activeItem, prop.FLEX_GROW)}
            {...this.props}
          />
          <FieldNumberItem
            label={prop.FLEX_SHRINK}
            value={items[activeItem][prop.FLEX_SHRINK]}
            onChange={handleChangeItem(activeItem, prop.FLEX_SHRINK)}
            {...this.props}
          />
        </form>

        <Typography variant="body1" className={classes.subtitles}>
          <InfoButton link="#article-header-id-12" />
          {`flex-basis: <length />; | auto; /* default auto */`}
        </Typography>
        <form className={classes.flex} noValidate autoComplete="off">
          {showNumberInput(items[activeItem][prop.FLEX_BASIS]) && (
            <FieldNumberItem
              label={prop.FLEX_BASIS_LENGTH}
              value={items[activeItem][prop.FLEX_BASIS_LENGTH]}
              onChange={handleChangeItem(activeItem, prop.FLEX_BASIS_LENGTH)}
              {...this.props}
            />
          )}
          <FieldSelectItem
            label={prop.FLEX_BASIS}
            selectItems={units}
            value={items[activeItem][prop.FLEX_BASIS]}
            onChange={handleChangeItem(activeItem, prop.FLEX_BASIS)}
            {...this.props}
          />
        </form>

        <Typography variant="body1" className={classes.subtitles}>
          <InfoButton link="#article-header-id-14" />
          align-self: auto | flex-start | flex-end | center | baseline |
          stretch;
        </Typography>
        <form className={classes.flex} noValidate autoComplete="off">
          <FieldSelectItem
            label={prop.ALIGN_SELF}
            selectItems={alignSelf}
            value={items[activeItem][prop.ALIGN_SELF]}
            onChange={handleChangeItem(activeItem, prop.ALIGN_SELF)}
            {...this.props}
          />
        </form>

        <Divider />
      </React.Fragment>
    );
  }
}

export default ItemFields;
