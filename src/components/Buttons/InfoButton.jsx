import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from '@material-ui/core/';
import IconButton from '@material-ui/core/IconButton';
import Info from '@material-ui/icons/InfoOutlined';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  infoButton: {
    padding: '0',
    marginRight: '0.3rem',
  },
});

const handleClickInfoButton = anchor => {
  const cssTricksUrl =
    'https://css-tricks.com/snippets/css/a-guide-to-flexbox/';
  window.open(`${cssTricksUrl + anchor}`, '_blank');
};

const InfoButton = ({ link, classes }) => (
  <Tooltip title="More explanation on css-tricks.com">
    <IconButton
      aria-label="More info"
      onClick={() => handleClickInfoButton(link)}
      color="primary"
      className={classes.infoButton}
    >
      <Info />
    </IconButton>
  </Tooltip>
);

InfoButton.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  link: PropTypes.string.isRequired,
};

export default withStyles(styles)(InfoButton);
