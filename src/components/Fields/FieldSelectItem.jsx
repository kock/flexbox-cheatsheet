import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core/';

const FieldSelectItem = ({ value, label, selectItems, classes, onChange }) => (
  <TextField
    id={label}
    select
    value={value}
    InputLabelProps={{
      shrink: true,
      'aria-label': { label },
    }}
    SelectProps={{
      native: true,
    }}
    margin="normal"
    className={classes.textFieldSelect}
    onChange={onChange}
  >
    {selectItems.map(option => (
      <option key={option} value={option}>
        {option}
      </option>
    ))}
  </TextField>
);

FieldSelectItem.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  selectItems: PropTypes.instanceOf(Array).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FieldSelectItem;
