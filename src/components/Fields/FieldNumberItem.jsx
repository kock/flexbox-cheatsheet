import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core/';

const FieldNumberItem = ({ classes, value, label, onChange }) => (
  <TextField
    id={label}
    value={value}
    type="number"
    InputLabelProps={{
      'aria-label': { label },
    }}
    margin="normal"
    className={classes.textField}
    onChange={onChange}
  />
);

FieldNumberItem.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  value: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FieldNumberItem;
