import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core/';

const FieldSelect = ({
  container,
  label,
  type,
  handleChange,
  selectItems,
  classes,
}) => (
  <TextField
    id={label}
    select
    value={container[type]}
    onChange={handleChange(type)}
    InputLabelProps={{
      shrink: true,
      'aria-label': { label },
    }}
    SelectProps={{
      native: true,
    }}
    margin="normal"
    className={classes.textFieldSelect}
  >
    {selectItems.map(option => (
      <option key={option} value={option}>
        {option}
      </option>
    ))}
  </TextField>
);

FieldSelect.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  container: PropTypes.instanceOf(Object).isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  selectItems: PropTypes.instanceOf(Array).isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default FieldSelect;
