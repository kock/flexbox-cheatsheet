import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/Root/App';
import registerServiceWorker from './registerServiceWorker';

const root = React.createElement(Root);
ReactDOM.render(root, document.getElementById('root'));
registerServiceWorker();
