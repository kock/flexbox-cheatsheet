import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Drawer } from '@material-ui/core/';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import ContainerFields from '../../components/AsideBar/ContainerFields/ContainerFields';
import ItemFields from '../../components/AsideBar/ItemFields/ItemFields';
import styles from '../../components/AsideBar/ContainerFields/styles';

class AsideBar extends Component {
  state = {
    expanded: null,
  };

  componentDidUpdate(prevProps) {
    const { items, activeItem } = this.props;
    if (prevProps.items[prevProps.activeItem] !== items[activeItem]) {
      this.expandPanel('panel2');
    }
  }

  handleChangePanel = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  expandPanel = panel => {
    this.setState({
      expanded: panel,
    });
  };

  render() {
    const {
      classes,
      items,
      container,
      activeItem,
      handleChange,
      handleChangeItem,
    } = this.props;
    const { expanded } = this.state;

    return (
      <Drawer
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        className={classes.asideBarContainer}
        anchor="right"
      >
        <ExpansionPanel
          expanded={expanded === 'panel1'}
          onChange={this.handleChangePanel('panel1')}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>
              Flexbox container properties
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.expansionPanel}>
            <section className={classes.wrapper}>
              <ContainerFields
                classes={classes}
                container={container}
                activeItem={activeItem}
                handleChange={handleChange}
                handleChangeItem={handleChangeItem}
              />
            </section>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel
          expanded={expanded === 'panel2'}
          onChange={this.handleChangePanel('panel2')}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>
              Flexbox item properties
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.expansionPanel}>
            {items[activeItem] ? (
              <section className={classes.wrapper}>
                <ItemFields
                  classes={classes}
                  items={items}
                  activeItem={activeItem}
                  handleChangeItem={handleChangeItem}
                />
              </section>
            ) : (
              <Typography variant="body2" className={classes.subtitles}>
                {`Want to change the properties of a flexbox item?
                      Just select one!`}
              </Typography>
            )}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </Drawer>
    );
  }
}

AsideBar.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  activeItem: PropTypes.number,
  items: PropTypes.instanceOf(Array).isRequired,
  container: PropTypes.instanceOf(Object).isRequired,
  handleChange: PropTypes.func.isRequired,
  handleChangeItem: PropTypes.func.isRequired,
};

AsideBar.defaultProps = { activeItem: null };

export default withStyles(styles)(AsideBar);
