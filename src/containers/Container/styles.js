const styles = theme => ({
  container: {
    height: '100%',
    position: 'relative',
    overflow: 'auto',
    maxWidth: '100vw',
    [theme.breakpoints.up('md')]: {
      maxWidth: '70vw',
    },
  },
  containerItem: {
    padding: '2rem 4rem',
    boxSizing: 'border-box',
    color: '#fff',
    background: '#333',
    border: '1px solid #fff',
    fontSize: '20px',
    cursor: 'pointer',
    '&:hover': {
      background: '#00bcd4',
    },
    '&.active': {
      background: '#00bcd4',
    },
  },
  delete: {
    background: '#fafafa',
    '&:hover': {
      background: '#fafafa',
    },
  },
  absolute: {
    position: 'fixed',
    top: '1em',
    left: '1em',
  },
});

export default styles;
