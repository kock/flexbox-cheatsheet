import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Paper from '@material-ui/core/Paper';

/* style imports */
import { inlineCSSStylingItem } from '../../utils/functions';
import styles from './styles';

const Container = ({
  classes,
  items,
  activeItem,
  handleSelect,
  handleAddItem,
  handleRemoveItem,
}) => (
  <main className={classes.main}>
    <section className={classNames(classes.container, 'flex-container')}>
      {items.map((item, index) => (
        <Paper
          onClick={handleSelect(index)}
          key={item.index}
          style={inlineCSSStylingItem(item)}
          className={classNames(
            classes.containerItem,
            `flex-item-${index + 1}`,
            {
              active: activeItem === index,
            },
          )}
        >
          {index + 1}
        </Paper>
      ))}
      <div className={classes.absolute}>
        <Tooltip title="Add">
          <Button
            variant="fab"
            color="secondary"
            aria-label="Add"
            onClick={handleAddItem}
          >
            <AddIcon />
          </Button>
        </Tooltip>
        <Tooltip title="Delete">
          <IconButton
            aria-label="Delete"
            onClick={handleRemoveItem}
            className={classes.delete}
          >
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </div>
    </section>
  </main>
);

Container.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  activeItem: PropTypes.number,
  items: PropTypes.instanceOf(Array).isRequired,
  handleSelect: PropTypes.func.isRequired,
  handleAddItem: PropTypes.func.isRequired,
  handleRemoveItem: PropTypes.func.isRequired,
};

Container.defaultProps = { activeItem: null };

export default withStyles(styles)(Container);
