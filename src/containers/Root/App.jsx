import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';
import AsideBar from '../AsideBar/AsideBarContainer';
import Container from '../Container/ItemsContainer';
import * as prop from '../../utils/definitions';

/* style imports */
import styles from './styles';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      container: {
        [prop.FLEX_DIRECTION]: 'row',
        [prop.FLEX_WRAP]: 'nowrap',
        [prop.JUSTIFY_CONTENT]: 'flex-start',
        [prop.ALIGN_ITEMS]: 'stretch',
        [prop.ALIGN_CONTENT]: 'stretch',
      },
      items: [
        {
          index: 1,
          [prop.ORDER]: 0,
          [prop.FLEX_GROW]: 0,
          [prop.FLEX_SHRINK]: 1,
          [prop.FLEX_BASIS_LENGTH]: 0,
          [prop.FLEX_BASIS]: 'auto',
          [prop.ALIGN_SELF]: 'auto',
          [prop.FLEX_ITEM_HEIGHT_LENGTH]: 100,
          [prop.FLEX_ITEM_HEIGHT]: 'auto',
          [prop.FLEX_ITEM_WIDTH_LENGTH]: 100,
          [prop.FLEX_ITEM_WIDTH]: 'auto',
        },
      ],
      activeItem: null,
    };
  }

  handleChange = name => event => {
    const { container } = this.state;
    container[name] = event.target.value;
    this.setState({ container });
  };

  handleChangeItem = (active, name) => event => {
    const { items } = this.state;
    items[active][name] = event.target.value;
    this.setState({ items });
  };

  handleSelect = index => () => {
    this.setState({ activeItem: index });
  };

  handleAddItem = () => {
    const { items } = this.state;

    items.push({
      index: items.length + 1,
      [prop.ORDER]: 0,
      [prop.FLEX_GROW]: 0,
      [prop.FLEX_SHRINK]: 1,
      [prop.FLEX_BASIS_LENGTH]: 0,
      [prop.FLEX_BASIS]: 'auto',
      [prop.ALIGN_SELF]: 'auto',
      [prop.FLEX_ITEM_HEIGHT_LENGTH]: 100,
      [prop.FLEX_ITEM_HEIGHT]: 'auto',
      [prop.FLEX_ITEM_WIDTH_LENGTH]: 100,
      [prop.FLEX_ITEM_WIDTH]: 'auto',
    });
    this.setState({ items });
  };

  handleRemoveItem = () => {
    const { items } = this.state;
    items.pop();
    this.setState({ items });
  };

  render() {
    const { classes } = this.props;
    const { container, items, activeItem } = this.state;

    return (
      <React.Fragment>
        <CssBaseline />
        <div className={classes.app}>
          <Container
            container={container}
            items={items}
            activeItem={activeItem}
            handleSelect={this.handleSelect}
            handleAddItem={this.handleAddItem}
            handleRemoveItem={this.handleRemoveItem}
          />
          <AsideBar
            container={container}
            items={items}
            activeItem={activeItem}
            handleChangeItem={this.handleChangeItem}
            handleChange={this.handleChange}
          />
        </div>
      </React.Fragment>
    );
  }
}

App.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default withStyles(styles)(App);
