const styles = theme => ({
  app: {
    minHeight: '100vh',
    display: 'grid',
    fontFamily: 'Roboto, sans-serif',
    position: 'relative',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 1fr',
    gridTemplateAreas: '"main""sidebar"',
    [theme.breakpoints.up('md')]: {
      gridTemplateColumns: '7fr 3fr',
      gridTemplateRows: '1fr',
      gridTemplateAreas: '"main sidebar"',
    },
  },
});

export default styles;
