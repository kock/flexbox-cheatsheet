import * as prop from './definitions';

export const inlineCSSStyling = flexbox => ({
  flexDirection: flexbox[prop.FLEX_DIRECTION],
  flexWrap: flexbox[prop.FLEX_WRAP],
  justifyContent: flexbox[prop.JUSTIFY_CONTENT],
  alignItems: flexbox[prop.ALIGN_ITEMS],
  alignContent: flexbox[prop.ALIGN_CONTENT],
});

export const displayCodeBlock = code =>
  `.flex-container {
    display: flex;
    flex-direction: ${code.flexDirection};
    flex-wrap: ${code.flexWrap};
    justify-content: ${code.justifyContent};
    align-items: ${code.alignItems};
    align-content: ${code.alignContent};
}
`;

export const showNumberInput = selectedValue =>
  selectedValue !== 'auto' &&
  selectedValue !== 'min-content' &&
  selectedValue !== 'max-content' &&
  selectedValue !== 'fit-content';

const multiField = (numberValue, selectedValue) => {
  if (showNumberInput(selectedValue)) return numberValue + selectedValue;

  return selectedValue;
};

export const inlineCSSStylingItem = index => ({
  height: multiField(
    index[prop.FLEX_ITEM_HEIGHT_LENGTH],
    index[prop.FLEX_ITEM_HEIGHT],
  ),
  width: multiField(
    index[prop.FLEX_ITEM_WIDTH_LENGTH],
    index[prop.FLEX_ITEM_WIDTH],
  ),
  order: index[prop.ORDER],
  flexGrow: index[prop.FLEX_GROW],
  flexShrink: index[prop.FLEX_SHRINK],
  flexBasis: multiField(index[prop.FLEX_BASIS_LENGTH], index[prop.FLEX_BASIS]),
  alignSelf: index[prop.ALIGN_SELF],
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
});

export const displayCodeBlockItem = item => {
  const itemField = inlineCSSStylingItem(item);

  return `.flex-item-${item.index} {
	order: ${item[prop.ORDER]};
	flex-grow: ${item[prop.FLEX_GROW]};
	flex-shrink: ${item[prop.FLEX_SHRINK]};
	flex-basis: ${itemField.flexBasis};
	align-self: ${item[prop.ALIGN_SELF]};
	
	height: ${itemField.height};
	width: ${itemField.width};
	
	display: 'flex',
	align-items: 'center',
	justify-content: 'center',
}`;
};
