export const FLEX_DIRECTION = 'flexDirection';
export const FLEX_WRAP = 'flexWrap';
export const JUSTIFY_CONTENT = 'justifyContent';
export const ALIGN_ITEMS = 'alignItems';
export const ALIGN_CONTENT = 'alignContent';

export const ORDER = 'order';
export const FLEX_GROW = 'flexGrow';
export const FLEX_SHRINK = 'flexShrink';
export const FLEX_BASIS = 'flexBasis';
export const FLEX_BASIS_LENGTH = 'flexBasisLength';
export const ALIGN_SELF = 'alignSelf';

export const FLEX_ITEM_HEIGHT_LENGTH = 'flexItemHeightLenght';
export const FLEX_ITEM_HEIGHT = 'flexItemHeight';
export const FLEX_ITEM_WIDTH_LENGTH = 'flexItemWidthLenght';
export const FLEX_ITEM_WIDTH = 'flexItemWidth';
