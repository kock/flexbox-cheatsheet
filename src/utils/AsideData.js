/* Flexbox container */
export const flexDirection = ['row', 'row-reverse', 'column', 'column-reverse'];
export const flexWrap = ['nowrap', 'wrap', 'wrap-reverse'];
export const justifyContent = [
  'flex-start',
  'flex-end',
  'center',
  'space-between',
  'space-around',
  'space-evenly',
];
export const alignItems = [
  'flex-start',
  'flex-end',
  'center',
  'baseline',
  'stretch',
];
export const alignContent = [
  'flex-start',
  'flex-end',
  'center',
  'space-between',
  'space-around',
  'stretch',
];

/* Flexbox items */
export const units = [
  '%',
  'px',
  'rem',
  'em',
  'vh',
  'vw',
  'auto',
  'min-content',
  'max-content',
  'fit-content',
];
export const alignSelf = [
  'auto',
  'flex-start',
  'flex-end',
  'center',
  'baseline',
  'stretch',
];
